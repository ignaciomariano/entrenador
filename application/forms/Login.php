<?php

class Application_Form_Login extends Zend_Form {

    public function init() {
        $this->addElement('text', 'email', array(
            'label' => 'Email',
            'required' => true
        ));
        $this->addElement('text', 'password', array(
            'label'=>'Password',
            'required'=>true
        ));
        $this->addElement('submit', 'login', array());
    }

}

?>
