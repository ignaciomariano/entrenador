<?php
class UsersController extends Zend_Controller_Action {
    public function listAction(){
        $model = new Application_Model_Users();
        $this->view->users = $model->getAll();
    }
    public function addAction(){
        $form = new Application_Form_Users();
        $this->view->form = $form;
    }
}
?>
