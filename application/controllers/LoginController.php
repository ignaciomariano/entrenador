<?php

class LoginController extends Zend_Controller_Action {

    public function init() {

        /* Initialize action controller here */
    }

    public function indexAction() {

        $form = new Application_Form_Login();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())){
                $this->view->msg = array('type'=>1, 'value'=>'Datos validos.');
                // para  grabar los datos del form:
                /*
                 * $model = new Application_Model_Users;
                 * $model->save($form->getValues());
                 * //redirect
                 * return $this->_redirect('/users/list');
                 */
                
            }else{
                $this->view->msg = array('type'=>0, 'value'=>'No se ha completado los datos necesarios.');
            }
        }
        $this->view->form = $form;
    }

    public function submitAction() {
        echo "Submit to Login";
        exit;
    }

    public function cancelAction() {
        echo "Cancel to Login";
        exit;
    }

}

